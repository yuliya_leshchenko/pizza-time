function goToInstagramByTag(tagName = "") {
  // const tagName = "pizzeria";
  window.open(`http://www.instagram.com/explore/tags/${tagName}/?hl=ru`);
}

function goToBlog() {
  const tagName = "pizzeria";
  window.open(`www.instagram.com/explore/tags/${tagName}/?hl=ru`);
}

function findByTag() {
  const tag = document.getElementById("tag").value;
  return goToInstagramByTag(tag);
}

const news = [];
let editNewsId = null;

function setInputsValues(obj = { title: "", text: "" }) {
  document.getElementById("title").value = obj.title;
  document.getElementById("text").value = obj.text;
}

function getNewsHtml(foundedNews) {
  return `
  <div id="${foundedNews.id}" class="white-area__blocks__text news-block" onclick="editNews(${foundedNews.id})">
    <p class="news-block__title white-area__blocks__text__orange">
      ${foundedNews.title}
    </p>
    <p class="news-block__text">
      ${foundedNews.text}
    </p>
    <button class="delete-button" onclick="deleteNews(${foundedNews.id})">[x]</button>
  </div>`;
}

function addNews() {
  const title = document.getElementById("title").value;
  const text = document.getElementById("text").value;
  const newsObject = {
    id: new Date().getTime(),
    title,
    text,
    date: new Date(),
  };
  if (!editNewsId) {
    news.push(newsObject);
    setInputsValues();
    return renderNews(newsObject.id);
  } else {
    const newsIndex = news.findIndex(function (item) {
      return editNewsId === item.id;
    });
    if (newsIndex !== -1) {
      news.splice(newsIndex, 1, newsObject);
      rerenderNews(newsObject.id);
      setInputsValues();
      editNewsId = null;
    }
  }
}

function rerenderNews(id) {
  const olderNews = document.getElementById(editNewsId);
  const foundedNews = news.find(function (item) {
    return id === item.id;
  });
  if (foundedNews && foundedNews.id) {
    olderNews.insertAdjacentHTML("beforebegin", getNewsHtml(foundedNews));
    olderNews.remove();
  }
}

function renderNews(id) {
  const newsArea = document.getElementsByClassName("js-practise")[0];
  const foundedNews = news.find(function (item) {
    return id === item.id;
  });
  if (foundedNews && foundedNews.id) {
    newsArea.insertAdjacentHTML("afterbegin", getNewsHtml(foundedNews));
  }
}

const editNews = (id) => {
  const selectedNews = news.find((item) => id === item.id);
  if (selectedNews && selectedNews.id) {
    setInputsValues({ title: selectedNews.title, text: selectedNews.text });
    editNewsId = selectedNews.id;
  }
};

const deleteNews = (id) => {
  const deleteNewsIndex = news.findIndex((item) => id === item.id);
  if (deleteNewsIndex !== -1) {
    news.splice(deleteNewsIndex, 1);
    const deletedNews = document.getElementById(id);
    deletedNews.remove();
  }
  window.event.cancelBubble = true;
};

const deleteAllNews = () => {
  const allNews = document.querySelector(".js-practise");
  allNews.innerHTML = "";
  news.length = 0;
};

let newsWithAuthor = [];
const addAuthor = () => {
  const author = prompt("enter your name", "");
  newsWithAuthor = news.map((item) => {
    return {
      id: item.id,
      title: item.title,
      text: item.text,
      date: item.date,
      author: author,
    };
  });
  deleteAllNews();
  renderAuthorNews();
};

const renderAuthorNews = () => {
  const newsArea = document.getElementsByClassName("js-practise")[0];
  newsWithAuthor.forEach(item => {
    news.push(item);
    newsArea.innerHTML += `
    <div id="${item.id}" class="white-area__blocks__text news-block" onclick="editNews(${item.id})">
      <p class="news-block__title white-area__blocks__text__orange">
        ${item.title}
      </p>
      <p class="news-block__text">
        ${item.text}
      </p>
      <code>${item.author}</code>
      <button class="delete-button" onclick="deleteNews(${item.id})">[x]</button>
    </div>`;
  })
}


